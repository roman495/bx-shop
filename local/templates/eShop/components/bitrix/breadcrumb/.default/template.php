<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(empty($arResult))
	return '';

global $APPLICATION;

$output = <<<START
<nav class="breadcrumb" aria-label="breadcrumbs">
  <ul>
START;

foreach ($arResult as $value) {
    if ($value == reset($arResult)) {
        // First element
        $breadcrumb_item = '<li>';
        $breadcrumb_item .= '<a href="' . $value['LINK'] . '"><span class="icon is-small"><i class="fas fa-home" aria-hidden="true"></i></span><span>Home</span></a>';
        $breadcrumb_item .= '</li>';    
    } elseif ($value == end($arResult)) {
        // Last element
        $breadcrumb_item = '<li class="is-active">';
        $breadcrumb_item .= '<a href="#" aria-current="page"><span>' . $value['TITLE'] . '</span></a>';
        $breadcrumb_item .= '</li>';
    } else {
        // Middle elements
        $breadcrumb_item = '<li>';
        $breadcrumb_item .= '<a href="' . $value['LINK'] . '"><span>' . $value['TITLE'] . '</span></a>';
        $breadcrumb_item .= '</li>';    
    }
    $output .= $breadcrumb_item;
}

$output .= <<<END
    </ul>
</nav>
END;

return $output;