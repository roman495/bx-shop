<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__DIR__);

$this->setFrameMode(true);
?>

<section class="section">
	<div class="columns">
		<div class="column is-one-quarter">
			<div class="menu">
				<p class="menu-label">
					<?=Loc::getMessage("CATEGORY");?>
				</p>
				<ul class="menu-list">
					<?php foreach( $arResult["SECTIONS"] as $section ) : ?>
						<li>

							<?php if( isset( $section['subitems'] ) ) : ?>
								<a href="<?=$section['SECTION_PAGE_URL']?>">
									<?=$section['NAME']?>
								</a>
								<ul>
									<?php foreach( $section['subitems'] as $subitem ) : ?>
										<a href="<?=$subitem['SECTION_PAGE_URL']?>">
											<?=$subitem['NAME']?>
											<span class="tag is-rounded is-info is-light"><?= $subitem['ELEMENT_CNT'] ?></span>
										</a>
									<?php endforeach ?>
								</ul>
							<?php else : ?>
								<a href="<?=$section['SECTION_PAGE_URL']?>">
									<?=$section['NAME']?>
									<span class="tag is-rounded is-info is-light"><?= $section['ELEMENT_CNT'] ?></span>
								</a>
							<?php endif ?>

						</li>
					<?php endforeach ?>
				</ul>
			</div>
		</div>
	</div>
</section>
