<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<div class="columns is-multiline">
	<?php foreach($arResult["ITEMS"] as $cell=>$arElement) : ?>
		<div class="column is-4">
			<div class="card">
				<div class="card-image">
					<figure class="image is-4by3">
						<a href="<?=$arElement["DETAIL_PAGE_URL"]?>" title="<?=$arElement["NAME"]?>">
							<img src="<?=$arElement["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arElement["NAME"]?>"/>
						</a>
					</figure>
				</div>
				<div class="card-content">
					<div class="content">
						<?php foreach($arElement["DISPLAY_PROPERTIES"] as $pid=>$arProperty) : ?>
							<?=$arProperty["NAME"]?>: <?
								if(is_array($arProperty["DISPLAY_VALUE"]))
									echo implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);
								else
									echo $arProperty["DISPLAY_VALUE"];?>
						<?php endforeach ?>
						
						<?=$arElement["PREVIEW_TEXT"]?>

						<?php if(is_array($arElement["OFFERS"]) && !empty($arElement["OFFERS"])):?>
							<?foreach($arElement["OFFERS"] as $arOffer):?>
								<?foreach($arOffer["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
									<?=$arProperty["NAME"]?>: <?
										if(is_array($arProperty["DISPLAY_VALUE"]))
											echo implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);
										else
											echo $arProperty["DISPLAY_VALUE"];?>
								<?endforeach?>
								<?foreach($arOffer["PRICES"] as $code=>$arPrice):?>
									<?if($arPrice["CAN_ACCESS"]):?>
										<?=$arResult["PRICES"][$code]["TITLE"];?>
										<?if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
											<s><?=$arPrice["PRINT_VALUE"]?></s> <?=$arPrice["PRINT_DISCOUNT_VALUE"]?>
										<?else:?>
											<?=$arPrice["PRINT_VALUE"]?>
										<?endif?>
									<?endif;?>
								<?endforeach;?>
								<?if($arOffer["CAN_BUY"]):?>
										<form action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
										<input type="text" name="<?echo $arParams["PRODUCT_QUANTITY_VARIABLE"]?>" value="1" size="5">
										<input type="hidden" name="<?echo $arParams["ACTION_VARIABLE"]?>" value="BUY">
										<input type="hidden" name="<?echo $arParams["PRODUCT_ID_VARIABLE"]?>" value="<?echo $arOffer["ID"]?>">
										<input type="submit" name="<?echo $arParams["ACTION_VARIABLE"]."BUY"?>" value="<?echo GetMessage("CATALOG_BUY")?>">
										<input type="submit" name="<?echo $arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" value="<?echo GetMessage("CATALOG_ADD")?>">
										</form>
								<?elseif(count($arResult["PRICES"]) > 0):?>
									<?=GetMessage("CATALOG_NOT_AVAILABLE")?>
								<?endif?>
							<?endforeach;?>
						<?else:?>
							<?foreach($arElement["PRICES"] as $code=>$arPrice):?>
								<?if($arPrice["CAN_ACCESS"]):?>
									<?=$arResult["PRICES"][$code]["TITLE"];?>:&nbsp;&nbsp;
									<?if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
										<s><?=$arPrice["PRINT_VALUE"]?></s> <?=$arPrice["PRINT_DISCOUNT_VALUE"]?>
									<?else:?>
										<?=$arPrice["PRINT_VALUE"]?>
									<?endif;?>
								<?endif;?>
							<?endforeach;?>
						
							<?if($arElement["CAN_BUY"]):?>
									<form action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
									<input type="text" name="<?echo $arParams["PRODUCT_QUANTITY_VARIABLE"]?>" value="1" size="5">
									<input type="hidden" name="<?echo $arParams["ACTION_VARIABLE"]?>" value="BUY">
									<input type="hidden" name="<?echo $arParams["PRODUCT_ID_VARIABLE"]?>" value="<?echo $arElement["ID"]?>">
									<input type="submit" name="<?echo $arParams["ACTION_VARIABLE"]."BUY"?>" value="<?echo GetMessage("CATALOG_BUY")?>">
									<input type="submit" name="<?echo $arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" value="<?echo GetMessage("CATALOG_ADD")?>">
									</form>
							<?elseif((count($arResult["PRICES"]) > 0) || is_array($arElement["PRICE_MATRIX"])):?>
								<?=GetMessage("CATALOG_NOT_AVAILABLE")?>
							<?endif?>
						<?endif?>
					</div>
				</div>
			</div>
		</div>
	<?php endforeach ?>
</div>

<?php if($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
	<?=$arResult["NAV_STRING"]?>
<?php endif;?>

