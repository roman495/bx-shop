<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<nav class="navbar" role="navigation" aria-label="main navigation">
  <div class="navbar-brand">
    <a class="navbar-item" href="/">
      <img src="<?=SITE_TEMPLATE_PATH?>/assets/i/shopping-bag.png">
      eShop
    </a>

    <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
    </a>
  </div>

  <div id="navbarBasicExample" class="navbar-menu">
    <div class="navbar-start">
        <?php foreach ($arResult as $key => $item): ?>
            <?php if ($item['DEPTH_LEVEL'] == 1): ?>
                <?php if ($item['IS_PARENT'] == 1): ?>
                    <div class="navbar-item has-dropdown is-hoverable">
                        <a class="navbar-link">
                            <?=$item['TEXT']?>
                        </a>
                        <div class="navbar-dropdown">
                            <?php foreach ($item['subitems'] as $subitem): ?>
                                <a class="navbar-item" href="<?=$subitem['LINK']?>">
                                    <?=$subitem['TEXT']?>
                                </a>
                            <?php endforeach ?>
                        </div>
                    </div>
                <?php else: ?>
                    <a class="navbar-item" href="<?=$item['LINK']?>">
                        <?=$item['TEXT']?>
                    </a>
                <?php endif ?>
            <?php endif ?>
        <?php endforeach ?>
    </div>
    <div class="navbar-end">
      <div class="navbar-item">
        <div class="buttons">
          <a class="button is-primary">
            <strong>Sign up</strong>
          </a>
          <a class="button is-light">
            Log in
          </a>
        </div>
      </div>
    </div>
  </div>
</nav>