<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!doctype html>
<html>
<head>
    <?php
    $APPLICATION->ShowHead();

    use Bitrix\Main\Page\Asset;

    Asset::getInstance()->addCss('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css');
    Asset::getInstance()->addCss('https://cdn.jsdelivr.net/npm/bulma@0.9.2/css/bulma.min.css');
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/assets/css/style.css');

    CJSCore::Init(array("jquery"));
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/assets/js/main.js');

    Asset::getInstance()->addString("<link rel='shortcut icon' href='/local/favicon.ico' />");
    Asset::getInstance()->addString("<meta name='viewport' content='width=device-width, initial-scale=1'>");
    Asset::getInstance()->addString("<link href='https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext' rel='stylesheet'>");
    Asset::getInstance()->addString("<link href='https://fonts.googleapis.com/css2?family=Montserrat:ital@1&display=swap' rel='stylesheet'>");
    ?>
	<title><?$APPLICATION->ShowTitle()?></title>
</head>
<body>
<?$APPLICATION->ShowPanel();?>

<header>
    <?php $APPLICATION->IncludeComponent('bitrix:menu', 'top_menu', [
        'ROOT_MENU_TYPE'        => 'top',
        'MAX_LEVEL'             => '2',
        'CHILD_MENU_TYPE'       => 'section',
        'USE_EXT'               => 'Y',
        'DELAY'                 => 'N',
        'ALLOW_MULTI_SELECT'    => 'Y',
        'MENU_CACHE_TYPE'       => 'N',
        'MENU_CACHE_TIME'       => '3600',
        'MENU_CACHE_USE_GROUPS' => 'Y',
        'MENU_CACHE_GET_VARS'   => ''
    ]);?>

    <section class="hero is-info">
        <div class="section">
            <div class="hero-body">
                <p class="title"><?php $APPLICATION->ShowTitle() ?></p>
                <p class="subtitle"><?= $APPLICATION->ShowProperty( 'description' ) ?></p>
            </div>
        </div>
    </section>

</header>

<div class="container">
    <div class="section">
        <?php if ($APPLICATION->GetCurPage() != '/') {
            $APPLICATION->IncludeComponent(
                'bitrix:breadcrumb',
                '',
                [
                    'START_FROM' => '0',
                    'SITE_ID'    => 's1',
                ]
            );
        } ?>
    </div>
